#include <iostream>
#include <fstream>

#include "../lib/parser/InputPosition.h"
#include "../lib/parser/ProxyBuffer.h"
#include "../lib/parser/Scanner.h"
#include "../lib/parser/Parser.h"
#include "../lib/parser/AnalysisErrors.h"
#include <list>
#include <exception>

using namespace parser;

int main(int argc, char* argv[])
{
    std::string in_filename, out_filename;
    if (argc >= 3) {
        in_filename = argv[1];
        out_filename = argv[2];
    } else {
        std::cout << "Input or output file unspecified";
        return 1;
    }
    std::fstream in(in_filename, std::fstream::in);
    std::fstream out(out_filename, std::fstream::out);
    if (!in.is_open() || !out.is_open())
    {
        std::cout << "File open error" << std::endl;
        return 1;
    }
    ProxyBuffer buffer(in);
    Scanner scanner(buffer);
    Parser parser(scanner);

    try {
        auto structure = parser.parse();
        std::cout<<structure->toString()<<std::endl;
        structure->fill(out);
    } catch (std::runtime_error e) {
        std::cout<<e.what();
        std::cout<<std::endl;
        return 1;
    }

    return 0;
}
