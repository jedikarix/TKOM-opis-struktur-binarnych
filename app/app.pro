TEMPLATE = app

TARGET =

CONFIG += console c++14
CONFIG -= lib_bundle
CONFIG -= qt

SOURCES += \
        main.cpp

unix:!macx: LIBS += -L$$OUT_PWD/../lib/ -llib

INCLUDEPATH += $$PWD/../lib
DEPENDPATH += $$PWD/../lib

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../lib/liblib.a
