#include "FieldType.h"

#include <map>

std::string ast::toString(ast::ValueType o)
{
    static std::map<ast::ValueType, std::string> strings = {
        {ast::ValueType::Int8, "int8"},
        {ast::ValueType::Int16, "int16"},
        {ast::ValueType::Int32, "int32"},
        {ast::ValueType::String, "str"}
    };
    return strings[o];
}
