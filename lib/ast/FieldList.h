#ifndef FIELDLIST_H
#define FIELDLIST_H

#include "Field.h"
#include <memory>
#include <list>

namespace ast {

class FieldList: public Field
{
public:
    FieldList();

    void fill(std::istream &in) override;
    void read(std::ostream &out) override;

    std::string toString() const override;

    void addField( std::unique_ptr<Field> field);

private:
    std::list< std::unique_ptr< Field > > fields_;
};

}
#endif // FIELDLIST_H
