#include "Structure.h"

using namespace ast;

Structure::Structure(std::string identifier, std::unique_ptr<FieldList> fields, std::shared_ptr<SymbolTable> table)
    :identifier_(identifier)
    ,fields_(std::move(fields))
    ,table_(table)
{
    identifier_ = identifier;
}

const std::string &Structure::getIdentifier() const
{
    return identifier_;
}

std::shared_ptr<SymbolTable> Structure::getSymbolTable() const
{
    return table_;
}

std::string Structure::toString() const
{
    return identifier_ + ": " + fields_->toString();
}

void Structure::fill(std::istream &in)
{
    fields_->fill(in);
}
