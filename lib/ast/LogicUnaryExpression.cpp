#include "LogicUnaryExpression.h"

using namespace ast;

LogicUnaryExpression::LogicUnaryExpression(Operator op,
                                           std::unique_ptr<LogicExpression> operand)
    :operator_(op)
    ,operand_(std::move(operand))
{}

bool LogicUnaryExpression::calculate() const
{
    bool val = operand_->calculate();

    return calculate(val);
}

std::string LogicUnaryExpression::toString() const
{
    return ast::toString(operator_) + operand_->toString();
}

bool LogicUnaryExpression::calculate(bool value) const
{
    if (operator_ == Operator::Negation)
        return !value;
    throw std::runtime_error("Wrong operator type. Logical operator expected. Given: " + ast::toString(operator_));
}
