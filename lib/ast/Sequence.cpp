#include "Sequence.h"

using namespace ast;

#include <iostream>

Sequence::Sequence(std::unique_ptr<ArithmExpression> length,
                   std::string ident,
                   std::unique_ptr<FieldList> fields,
                   std::shared_ptr<SymbolTable> table)
    :length_expr_(std::move(length))
    ,ident_(ident)
    ,fields_(std::move(fields))
    ,table_(table)
{}

void Sequence::fill(std::istream &infile)
{
    int length = length_expr_->calculate();
    std::cout<<ident_<<"["<<length<<"]:\n";
    for (int i = 0; i < length; ++i) {
        std::cout<<"["<<i<<"]: ";
        fields_->fill(infile);
    }
}

void Sequence::read(std::ostream &outfile)
{
    int length = length_expr_->calculate();
    for (int i = 0; i < length; ++i) {
        fields_->read(outfile);
    }
}

std::string Sequence::toString() const
{
    return "[" + length_expr_->toString() + "] " +
            ident_ + ": " + fields_->toString();
}

