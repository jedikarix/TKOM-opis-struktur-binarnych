#include "File.h"

using namespace ast;

File::File(std::unique_ptr<StructureList> structures,
           std::unique_ptr<Structure> main_structure,
           std::shared_ptr<SymbolTable> table)
    :structures_(std::move(structures))
    ,main_structure_(std::move(main_structure))
    ,table_(table)
{}

std::string File::toString() const
{
    const std::string a = structures_->toString();
    const std::string b = main_structure_->toString();
    return a+b;
}

void File::fill(std::istream &infile)
{
    main_structure_->fill(infile);
}
