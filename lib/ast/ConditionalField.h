#ifndef CONDITIONALFIELD_H
#define CONDITIONALFIELD_H

#include "Field.h"
#include "FieldList.h"
#include "LogicExpression.h"
#include "SymbolTable.h"

#include <memory>

namespace ast {

class ConditionalField: public Field
{
public:
    ConditionalField(std::unique_ptr<LogicExpression> condition,
                     std::unique_ptr<FieldList> fields,
                     std::shared_ptr<SymbolTable> table);

    void fill(std::istream &in) override;
    void read(std::ostream &out) override;

    std::string toString() const override;
private:
    std::unique_ptr<LogicExpression> condition_;
    std::unique_ptr<FieldList> fields_;
    std::shared_ptr<SymbolTable> table_;
};

}
#endif // CONDITIONALFIELD_H
