#include "Operator.h"

#include <map>

#include <set>

using namespace ast;


std::string ast::toString(Operator o)
{
    static std::map<Operator, std::string> symbols {
        {Operator::Plus,            "+"},
        {Operator::Minus,           "-"},
        {Operator::Multiplication,  "*"},
        {Operator::Ceil_division,   "/+"},
        {Operator::Floor_division,  "/-"},
        {Operator::Modulo_division, "%"},
        {Operator::Greater,         ">"},
        {Operator::Greater_equal,   ">="},
        {Operator::Less,            "<"},
        {Operator::Less_equal,      "<="},
        {Operator::Not_equal,       "!="},
        {Operator::Equal,           "=="},
        {Operator::Disjunction,     "||"},
        {Operator::Conjunction,     "&&"},
        {Operator::Negation,        "!"},
        {Operator::Reference,       "->"},
        {Operator::Dot,             "."},
        {Operator::Colon,           ":"},
        {Operator::Comma,           ","},
        {Operator::Cond_mark,       "?"},
        {Operator::Left_bracket,    "("},
        {Operator::Right_bracket,   ")"},
        {Operator::Left_sq_bracket, "["},
        {Operator::Right_sq_bracket,"]"}
    };
    return symbols[o];
}

bool isAdditional(Operator o)
{
    return o == Operator::Plus || o == Operator::Minus;
}

bool isMultiplicative(Operator o)
{
    return o == Operator::Multiplication ||
           o == Operator::Ceil_division  ||
           o == Operator::Floor_division ||
           o == Operator::Modulo_division;
}

bool isRelational(Operator o)
{
    return o == Operator::Equal      ||
           o == Operator::Not_equal  ||
           o == Operator::Greater    ||
           o == Operator::Greater_equal ||
           o == Operator::Less       ||
           o == Operator::Less_equal;
}
