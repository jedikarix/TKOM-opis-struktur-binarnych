#ifndef BOOLVALUE_H
#define BOOLVALUE_H

#include "LogicExpression.h"

namespace ast {

class BoolValue: public LogicExpression
{
public:
    BoolValue(bool value);

    bool calculate() const override;
    std::string toString() const override;

private:
    bool value_;
};

}

#endif // BOOLVALUE_H
