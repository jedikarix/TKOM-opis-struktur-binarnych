#include "StandardField.h"

using namespace ast;

StandardField::StandardField(const std::string& ident, ValueType type)
    :identifier_(ident)
    ,type_(type)
{
}

const std::string &StandardField::getIdentifier() const
{
    return identifier_;
}
