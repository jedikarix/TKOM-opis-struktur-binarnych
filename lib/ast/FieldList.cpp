#include "FieldList.h"

using namespace ast;

FieldList::FieldList()
{

}

void FieldList::fill(std::istream &in)
{
    for (auto it = fields_.begin(); it != fields_.end(); ++it) {
        (*it)->fill(in);
    }
}

void FieldList::read(std::ostream &out)
{
    for (auto it = fields_.begin(); it != fields_.end(); ++it) {
        (*it)->read(out);
    }
}

std::string FieldList::toString() const
{
    std::string ret;
    if (fields_.empty())
        return ".";
    auto it = fields_.begin();
    ret += (*it)->toString();
    ++it;
    for (; it != fields_.end(); ++it) {
        ret += ", " + (*it)->toString();
    }
    return ret + ".";
}

void FieldList::addField(std::unique_ptr<Field> field)
{
    fields_.push_back(std::move(field));
}
