#include "IntVariable.h"

using namespace ast;

IntVariable::IntVariable(std::string ident, std::shared_ptr<SymbolTable> sym_table)
    :identifier_(ident)
    ,value_ref_(sym_table->getIntRef(ident))
{}

int IntVariable::calculate() const
{
    return value_ref_;
}

std::string IntVariable::toString() const
{
    return identifier_;
}
