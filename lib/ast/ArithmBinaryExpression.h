#ifndef BINARYEXPRESSION_H
#define BINARYEXPRESSION_H

#include <memory>

#include "ArithmExpression.h"
#include "Operator.h"

namespace ast {

class ArithmBinaryExpression: public ArithmExpression
{
public:
    ArithmBinaryExpression(std::unique_ptr<ArithmExpression> left_operand,
                     Operator op,
                     std::unique_ptr<ArithmExpression> right_operand);

    int calculate() const override;
    std::string toString() const override;

private:
    virtual int calculate(int left_value, int right_value) const;

    std::unique_ptr<ArithmExpression> left_operand_;
    Operator operator_;
    std::unique_ptr<ArithmExpression> right_operand_;

};

}

#endif // BINARYEXPRESSION_H
