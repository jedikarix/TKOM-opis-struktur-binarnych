#ifndef STANDARDFIELD_H
#define STANDARDFIELD_H

#include "FieldType.h"
#include "Field.h"
#include "Identifier.h"
#include "SymbolTable.h"

namespace ast {

class StandardField: public Field

{
public:
    StandardField(const std::string &ident, ast::ValueType type);

    const std::string& getIdentifier() const;

protected:
    ast::ValueType type_;

private:
    std::string identifier_;
};

}
#endif // STANDARDFIELD_H
