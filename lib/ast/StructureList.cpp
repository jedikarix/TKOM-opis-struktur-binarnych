#include "StructureList.h"

using namespace ast;

StructureList::StructureList()
{
    structures_.clear();
}

std::string StructureList::toString() const
{
    if (structures_.size() == 0)
        return "";
    std::string ret;
    for (auto it = structures_.begin(); it != structures_.end(); ++it) {
        ret += (*it)->toString();
    }
    return ret;
}

void StructureList::addStructure(std::unique_ptr<Structure> structure)
{
    structures_.push_back(std::move(structure));
}
