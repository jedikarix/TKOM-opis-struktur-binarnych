#ifndef INTVALUE_H
#define INTVALUE_H

#include "ArithmExpression.h"

namespace ast {

class IntValue : public ArithmExpression
{
public:
    IntValue(int value);
    int calculate() const override;
    std::string toString() const override;

private:
    int value_;
};

}

#endif // INTVALUE_H
