#ifndef OPERATOR_H
#define OPERATOR_H

#include <string>

namespace ast{

enum class Operator {
    Plus,
    Minus,
    Multiplication,
    Ceil_division,
    Floor_division,
    Modulo_division,
    Greater,
    Greater_equal,
    Less,
    Less_equal,
    Not_equal,
    Equal,
    Disjunction,
    Conjunction,
    Negation,
    Reference,
    Dot,
    Colon,
    Comma,
    Cond_mark,
    Left_bracket,
    Right_bracket,
    Left_sq_bracket,
    Right_sq_bracket
};

std::string toString(Operator o);
bool isAdditional(Operator o);
bool isMultiplicative(Operator o);
bool isRelational(Operator o);
inline std::ostream& operator<<(std::ostream& out, Operator o) {return out << toString(o);}

}

#endif // OPERATOR_H
