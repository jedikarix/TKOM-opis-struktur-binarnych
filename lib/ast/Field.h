#ifndef FIELD_H
#define FIELD_H

#include <ostream>
#include <istream>

namespace ast {

class Field
{
public:
    Field();

    virtual void fill(std::istream &in) = 0;
    virtual void read(std::ostream &in) = 0;

    virtual std::string toString() const = 0;
};

}

#endif // FIELD_H
