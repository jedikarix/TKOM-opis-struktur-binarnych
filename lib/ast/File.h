#ifndef FILE_H
#define FILE_H

#include "StructureList.h"

namespace ast {

class File
{
public:
    File(std::unique_ptr<StructureList> structures,
         std::unique_ptr<Structure> main_structure,
         std::shared_ptr<SymbolTable> table);

    std::string toString() const;

    void fill(std::istream& infile);
    void read(std::ostream& outfile);
private:
    std::unique_ptr<StructureList> structures_;
    std::unique_ptr<Structure> main_structure_;
    std::shared_ptr<SymbolTable> table_;
};

}

#endif // FILE_H
