#include "RelationalExpression.h"

using namespace ast;

RelationalExpression::RelationalExpression(std::unique_ptr<ArithmExpression> left_operand,
                                           Operator op,
                                           std::unique_ptr<ArithmExpression> right_operand)
    :left_operand_(std::move(left_operand))
    ,operator_(op)
    ,right_operand_(std::move(right_operand))
{}

bool RelationalExpression::calculate() const
{
    int left_val = left_operand_->calculate();
    int right_val = right_operand_->calculate();

    return calculate(left_val, right_val);
}

std::string RelationalExpression::toString() const
{
    return left_operand_->toString() + ast::toString(operator_) + right_operand_->toString();
}

bool RelationalExpression::calculate(int left_value, int right_value) const
{
    static std::map<Operator, std::function<bool(int,int)> > operations {
        {Operator::Equal,               [](int a, int b){return a == b;}},
        {Operator::Not_equal,           [](int a, int b){return a != b;}},
        {Operator::Greater,             [](int a, int b){return a > b;}},
        {Operator::Greater_equal,       [](int a, int b){return a >= b;}},
        {Operator::Less,                [](int a, int b){return a < b;}},
        {Operator::Less_equal,          [](int a, int b){return a <= b;}}
    };
    auto operation = operations.find(operator_);
    if (operation == operations.end()) {
        throw std::runtime_error("Wrong operator type. Relational operator excepted. Given: " + ast::toString(operator_));
    }
    return operations[operator_](left_value, right_value);
}
