TEMPLATE = lib
CONFIG += staticlib

CONFIG += c++14
CONFIG -= lib_bundle


SOURCES += \
    ast/ArithmExpression.cpp \
    ast/FieldType.cpp \
    ast/Identifier.cpp \
    ast/Operator.cpp \
    parser/AnalysisErrors.cpp \
    parser/InputPosition.cpp \
    parser/ProxyBuffer.cpp \
    parser/Scanner.cpp \
    parser/Token.cpp \
    ast/LogicExpression.cpp \
    ast/BoolValue.cpp \
    ast/RelationalExpression.cpp \
    ast/LogicBinaryExpression.cpp \
    parser/Parser.cpp \
    ast/LogicUnaryExpression.cpp \
    ast/ArithmBinaryExpression.cpp \
    ast/Field.cpp \
    ast/StandardField.cpp \
    ast/IntField.cpp \
    ast/SymbolTable.cpp \
    ast/Structure.cpp \
    ast/IntValue.cpp \
    ast/IntVariable.cpp \
    ast/FieldList.cpp \
    ast/ConditionalField.cpp \
    ast/Sequence.cpp \
    ast/File.cpp \
    ast/StructureList.cpp

HEADERS += \
    ast/ArithmExpression.h \
    ast/FieldType.h \
    ast/Identifier.h \
    ast/Operator.h \
    parser/AnalysisErrors.h \
    parser/InputPosition.h \
    parser/ProxyBuffer.h \
    parser/Scanner.h \
    parser/Token.h \
    ast/LogicExpression.h \
    ast/BoolValue.h \
    ast/RelationalExpression.h \
    ast/LogicBinaryExpression.h \
    parser/Parser.h \
    ast/LogicUnaryExpression.h \
    ast/ArithmBinaryExpression.h \
    ast/Field.h \
    ast/StandardField.h \
    ast/IntField.h \
    ast/SymbolTable.h \
    ast/Structure.h \
    ast/IntValue.h \
    ast/IntVariable.h \
    ast/FieldList.h \
    ast/ConditionalField.h \
    ast/Sequence.h \
    ast/File.h \
    ast/StructureList.h
