#ifndef INPUTPOSITION_H
#define INPUTPOSITION_H

#include <string>
namespace parser {

class InputPosition
{
public:

    struct UserFriendlyPos {
        UserFriendlyPos()
            :line_(1), column_(1) {}
        UserFriendlyPos(unsigned line, unsigned column)
            :line_(line), column_(column) {}
        inline std::string toString() const
        {
            return "(" + std::to_string(line_) + ", " + std::to_string(column_) + ")";
        }
        unsigned line_;
        unsigned column_;
    };

    InputPosition();
    void nextChar();
    void nextLine();
    UserFriendlyPos getUFPos() const;
    unsigned getCharNb() const;

private:
    UserFriendlyPos uf_pos_;
    unsigned pos_;
};

inline std::ostream& operator<<(std::ostream& o, InputPosition::UserFriendlyPos uf_position)
{
    return o << uf_position.toString();
}

}

#endif // INPUTPOSITION_H
