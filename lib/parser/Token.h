#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <new>

#include "../ast/Operator.h"
#include "../ast/FieldType.h"
#include "../ast/Identifier.h"
#include "InputPosition.h"

namespace parser {

class Token
{
public:

    enum class Type {
        Dot,                // .
        Colon,              // :
        Comma,              // ,
        L_bracket,          // (
        R_bracket,          // )
        Sq_L_bracket,       // [
        Sq_R_bracket,       // ]
        Cond_mark,          // ?
        Rel_op,             // > >= < <= != ==
        Disj_op,            // ||
        Conj_op,            // &&
        Add_op,             // + -
        Mul_op,             // * /- /+ %
        Logic_neg_op,       // !
        Ref_op,             // ->
        Int_val,            // 0|[1-9][0-9]*
        Bool_val,           // True False
        Ident,              // [a-zA-z][a-zA-Z0-9]*
        Type_int,           // int8 int16 int32
        Type_string,        // str
        Main,               // Main
        Eof
    };

    Token();
    Token(int);
    Token(bool);
    Token(const char* v);
    Token(std::string v);
    Token(ast::Main v);
    Token(ast::Operator v);
    Token(ast::ValueType v);

    Type getType() const;

    int getIntVal() const;
    bool getBoolVal() const;
    std::string getIdentifier() const;
    ast::Operator getOperator() const;
    ast::ValueType getValueType() const;

    static Type operatorToType(ast::Operator oper);
    static Type valueTypeToType(ast::ValueType type);

    static std::string toString(Token::Type type);
    std::string toString();

    void setPosition(InputPosition pos);
    InputPosition getPosition() const;

private:

    Type type_;

    union Value{
        int                     int_val;
        bool                    bool_val;
        ast::Operator           operator_val;
        ast::ValueType          type_val;
    } value_;

    std::string id_val_;

    InputPosition position_;
};

inline std::ostream& operator<<(std::ostream& o, Token::Type type)
{
    return o << Token::toString(type);
}

}

#endif // TOKEN_H
