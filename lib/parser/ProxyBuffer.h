#ifndef BUFFER_H
#define BUFFER_H

#include <string>
#include <istream>

#include "InputPosition.h"

namespace parser {

class ProxyBuffer
{

public:
    ProxyBuffer(std::istream& in);

    char get();
    char peek();
    void ignoreWhitespaces();
    bool eof();
    InputPosition getInputPos() const;

private:
    std::istream& in_;
    InputPosition input_pos_;

    void updateInputPos(char ch);

};

}

#endif // BUFFER_H
