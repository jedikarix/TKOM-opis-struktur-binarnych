#ifndef ANALYSISERRORS_H
#define ANALYSISERRORS_H

#include "InputPosition.h"

namespace parser {

class ScannerException
{
public:
    enum class Type {
        INT_OVERFLOW,
        IDENTIFIER_TOO_LONG,
        TOKEN_UNRECOGNISED
    };

    ScannerException(Type type, InputPosition pos);

    std::string what() const;

private:
    Type type_;
    InputPosition position_;
};

}

#endif // ANALYSISERRORS_H
