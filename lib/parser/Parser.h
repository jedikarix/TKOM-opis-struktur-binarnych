#ifndef PARSER_H
#define PARSER_H

#include <memory>

#include "Scanner.h"
#include "../ast/BoolValue.h"
#include "../ast/LogicUnaryExpression.h"
#include "../ast/LogicBinaryExpression.h"
#include "../ast/RelationalExpression.h"
#include "../ast/ArithmBinaryExpression.h"
#include "../ast/IntValue.h"
#include "../ast/IntField.h"
#include "../ast/ConditionalField.h"
#include "../ast/Sequence.h"
#include "../ast/IntVariable.h"
#include "../ast/FieldList.h"
#include "../ast/Structure.h"
#include "../ast/StructureList.h"
#include "../ast/File.h"

using namespace ast;

namespace parser {

class Parser
{
public:
    Parser(Scanner& scanner);

    std::unique_ptr<File>               parse();

    std::unique_ptr<File>               readFile();
    std::unique_ptr<StructureList>      readStructureList();
    std::unique_ptr<Structure>          readStructure();
    std::unique_ptr<Structure>          readMainStructure();
    std::unique_ptr<FieldList>          readFieldList();
    std::unique_ptr<Field>              readField();
    std::unique_ptr<Field>              readIntField();
    std::unique_ptr<Field>              readConditionalField();
    std::unique_ptr<Field>              readSequence();
    std::unique_ptr<LogicExpression>    readLogicExpression();
    std::unique_ptr<LogicExpression>    readAlternative();
    std::unique_ptr<LogicExpression>    readConjunct();
    std::unique_ptr<LogicExpression>    readLogicLiteral();
    std::unique_ptr<LogicExpression>    readRelationalExpression();
    std::unique_ptr<ArithmExpression>   readArithmExpression();
    std::unique_ptr<ArithmExpression>   readTerm();
    std::unique_ptr<ArithmExpression>   readFactor();
    std::unique_ptr<ArithmExpression>   readLiteral();

private:

    std::unique_ptr<LogicExpression> readNegateConjunct();
    std::unique_ptr<LogicExpression> readLogicExpression(std::unique_ptr<LogicExpression> left_oper);
    std::unique_ptr<LogicExpression> readAlternative(std::unique_ptr<LogicExpression> left_oper);
    std::unique_ptr<ArithmExpression> readArithmExpression(std::unique_ptr<ArithmExpression> left_oper);
    std::unique_ptr<ArithmExpression> readTerm(std::unique_ptr<ArithmExpression> left_oper);
    std::unique_ptr<ArithmExpression> readVariable();
    bool checkTokenType(Token::Type type);

    std::string readIdentifier();

    Operator readOperator(Token::Type op);

    ValueType readIntFieldType();
    ValueType readStringFieldType();

    void readMain();
    int readIntValue();
    bool readBoolValue();
    Token requireToken(Token::Type expected);

    void advance();

    void throwOnUnexpectedInput(Token::Type expected);

    void setContext(std::shared_ptr<SymbolTable> context);
    void backContext();

    std::shared_ptr<SymbolTable> current_context_;
    Scanner& scanner_;
    Token curr_token_;




};

}
#endif // PARSER_H
