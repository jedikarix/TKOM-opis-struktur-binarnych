#include "InputPosition.h"

using namespace parser;

InputPosition::InputPosition()
    : pos_(1)
{}

void InputPosition::nextChar()
{
    ++uf_pos_.column_;
    ++pos_;
}

void InputPosition::nextLine()
{
    ++uf_pos_.line_;
    uf_pos_.column_ = 1;
}

InputPosition::UserFriendlyPos InputPosition::getUFPos() const
{
    return uf_pos_;
}

unsigned InputPosition::getCharNb() const
{
    return pos_;
}

