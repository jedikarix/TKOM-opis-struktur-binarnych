#include "ParsingTest.h"

void ParsingTest::init()
{
    buffer_ = new ProxyBuffer(in_);
    scanner_ = new Scanner(*buffer_);
    parser_ = new Parser(*scanner_);
}

void ParsingTest::cleanup()
{
    delete buffer_;
    delete scanner_;
    delete parser_;
}
