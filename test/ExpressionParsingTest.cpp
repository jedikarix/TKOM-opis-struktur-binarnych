#include "ExpressionParsingTest.h"

#include <QDebug>


void ExpressionParsingTest::parseIntValueDirectly()
{
    setInput("14");
    scanner_->readNextToken();
    const auto expr = parser_->readLiteral();
    QCOMPARE(expr->calculate(), 14);
}

void ExpressionParsingTest::parseIntValue()
{
    setInput("15");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 15);
}

void ExpressionParsingTest::parseProductOfTwoIntValuesDirectly()
{
    setInput("4*5");
    scanner_->readNextToken();
    const auto expr = parser_->readTerm();
    QCOMPARE(expr->calculate(), 20);
}

void ExpressionParsingTest::parseProductOfTwoIntValues()
{
    setInput("7*8");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 56);
}

void ExpressionParsingTest::parseSumOfTwoIntValue()
{
    setInput("7+8");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(),15);
}

void ExpressionParsingTest::parseSumOfIntValueAndMultiplicativeExpression()
{
    setInput("4+5*5");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 29);
}

void ExpressionParsingTest::parseSumOfTwoMultiplicativeExpression()
{
    setInput("7*5+3*3");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 44);
}

void ExpressionParsingTest::parseProductOfIntValueAndSum()
{
    setInput("10*(1+2)");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 30);
}

void ExpressionParsingTest::parseProductOfTwoSum()
{
    setInput("(7+6)*(3-1)");
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), 26);
}

void ExpressionParsingTest::parseComplexExpression()
{
    setInput("(1+2)*(3+4*(5-6))"); //3*(3+4*-1) = 3*-1 = -3
    scanner_->readNextToken();
    const auto expr = parser_->readArithmExpression();
    QCOMPARE(expr->calculate(), -3);
}



