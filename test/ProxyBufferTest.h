#ifndef PROXYBUFFERTEST_H
#define PROXYBUFFERTEST_H


#include <QObject>
#include <QtTest/QtTest>

#include "InputTest.h"
#include "../lib/parser/ProxyBuffer.h"

using namespace parser;

class ProxyBufferTest : public InputTest
{
    Q_OBJECT
public:
    ProxyBufferTest(){}

private:
    ProxyBuffer* buffer_;


private slots:
    void init();
    void cleanup();

    void testPeek();
    void testGetAfterPeek();
    void testGet();
    void testIgnoreWhitespaces();
    void testInputPos();
    void testEof();

};

#endif // PROXYBUFFERTEST_H
