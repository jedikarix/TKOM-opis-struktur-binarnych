#ifndef TOKENTEST_H
#define TOKENTEST_H

#include <QString>
#include <QtTest/QTest>

#include "../lib/parser/Token.h"

class TokenTest : public QObject
{
    Q_OBJECT

public:
    TokenTest();

private Q_SLOTS:
    void init();
    void initInt();
    void initBool();
    void initPlus();
    void initModulo();
    void initDisjunction();
    void initConjunction();
    void initGreaterOper();
    void initNegOper();
    void initRefOper();
    void initColon();
    void initComma();
    void initCondMark();
    void initDot();
    void initLeftBracket();
    void initRightBracket();
    void initSqLeftBracket();
    void initSqRightBracket();
    void initString();
    void initType();

};

#endif // TOKENTEST_H
