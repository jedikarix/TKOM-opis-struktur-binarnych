#ifndef EXPRESSIONPARSERTEST_H
#define EXPRESSIONPARSERTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "ParsingTest.h"

#include "../lib/parser/Parser.h"

using namespace parser;

class ExpressionParsingTest: public ParsingTest
{
    Q_OBJECT
public:
    ExpressionParsingTest(){}

private slots:

    void parseIntValueDirectly();
    void parseIntValue();
    void parseProductOfTwoIntValuesDirectly();
    void parseProductOfTwoIntValues();
    void parseSumOfTwoIntValue();
    void parseSumOfIntValueAndMultiplicativeExpression();
    void parseSumOfTwoMultiplicativeExpression();
    void parseProductOfIntValueAndSum();
    void parseProductOfTwoSum();
    void parseComplexExpression();
};

#endif // EXPRESSIONPARSERTEST_H
