#include "RelationalExpressionTest.h"

using namespace ast;

void RelationalExpressionTest::checkEqualForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Equal,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.calculate(), true);
    RelationalExpression exp2(std::make_unique<IntValue>(5),
                              Operator::Equal,
                              std::make_unique<IntValue>(6));
    QCOMPARE(exp2.calculate(), false);
}

void RelationalExpressionTest::checkNotEqualForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Not_equal,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.calculate(), false);
    RelationalExpression exp2(std::make_unique<IntValue>(5),
                              Operator::Not_equal,
                              std::make_unique<IntValue>(6));
    QCOMPARE(exp2.calculate(), true);
}

void RelationalExpressionTest::checkGreaterForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(6),
                             Operator::Greater,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.calculate(), true);
    RelationalExpression exp2(std::make_unique<IntValue>(5),
                              Operator::Greater,
                              std::make_unique<IntValue>(6));
    QCOMPARE(exp2.calculate(), false);
}

void RelationalExpressionTest::checkGreaterEqualForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Greater_equal,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.calculate(), true);
    RelationalExpression exp2(std::make_unique<IntValue>(5),
                              Operator::Greater_equal,
                              std::make_unique<IntValue>(6));
    QCOMPARE(exp2.calculate(), false);
}

void RelationalExpressionTest::checkLessForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Less,
                             std::make_unique<IntValue>(6));
    QCOMPARE(exp.calculate(), true);
    RelationalExpression exp2(std::make_unique<IntValue>(6),
                              Operator::Less,
                              std::make_unique<IntValue>(5));
    QCOMPARE(exp2.calculate(), false);
}

void RelationalExpressionTest::checkLessEqualForIntVals()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Less_equal,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.calculate(), true);
    RelationalExpression exp2(std::make_unique<IntValue>(6),
                              Operator::Less_equal,
                              std::make_unique<IntValue>(5));
    QCOMPARE(exp2.calculate(), false);
}

void RelationalExpressionTest::ifNonRelationalOperatorThrowException()
{
    RelationalExpression exp(std::make_unique<IntValue>(3),
                             Operator::Colon,
                             std::make_unique<IntValue>(4));
    QVERIFY_EXCEPTION_THROWN(exp.calculate(), std::runtime_error);
}

void RelationalExpressionTest::printsProperlyString()
{
    RelationalExpression exp(std::make_unique<IntValue>(5),
                             Operator::Less_equal,
                             std::make_unique<IntValue>(5));
    QCOMPARE(exp.toString(), "5<=5");
}






