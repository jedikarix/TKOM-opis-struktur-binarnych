#ifndef BINARYEXPRESSIONTESTS_H
#define BINARYEXPRESSIONTESTS_H

#include <QObject>
#include <QtTest/QtTest>

#include "../lib/ast/ArithmBinaryExpression.h"
#include "../lib/ast/IntValue.h"

class BinaryExpressionTest : public QObject
{
    Q_OBJECT
public:
    BinaryExpressionTest(){}

private slots:
    void sumTwoIntVals();
    void subtTwoIntVals();
    void multTwoIntVals();
    void ceilDivTwoIntVals();
    void floorDivTwoIntVals();
    void moduloDivTwoIntVals();
    void sumIntValAndBinaryExp();
    void sumTwoBinaryExp();
    void ifNonArithmOperatorThrowException();

};

#endif // BINARYEXPRESSIONTESTS_H
