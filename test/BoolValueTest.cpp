#include "BoolValueTest.h"

BoolValueTest::BoolValueTest()
{

}

void BoolValueTest::returnsGivenValue()
{
    const bool value = true;
    ast::BoolValue val(value);
    QCOMPARE(val.calculate(), value);
}

void BoolValueTest::printProperlyString()
{
    ast::BoolValue val(true);
    QCOMPARE(val.toString(), "True");
}
