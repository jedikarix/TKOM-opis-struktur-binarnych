#include "IntValueTest.h"

void IntValueTest::returnsGivenValue()
{
    const int value = 123;
    ast::IntValue val(value);
    QCOMPARE(val.calculate(), value);
}


void IntValueTest::printsProperlyString()
{
    ast::IntValue val(3333);
    QCOMPARE(val.toString(), "3333");
}
