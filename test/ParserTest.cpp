#include "ParserTest.h"

#include <QDebug>

void ParserTest::parseStructureWithOneField()
{
    setInput("Main: int8 a.");
    const auto struct_file = parser_->parse();
    QCOMPARE(struct_file->toString().c_str(), "Main: int8 a.");
}

void ParserTest::parseStructureWithIntFieldAndConditionField()
{
    setInput("Main: int8 a, (a > 3)?: int8 b, int8 c..");
    const auto struct_file = parser_->parse();
    QCOMPARE(struct_file->toString().c_str(), "Main: int8 a, (a>3)?: int8 b, int8 c..");
}

void ParserTest::parseStructureWithSequence()
{
    setInput("Main: [3*3]a: int8 b, int8 c..");
    const auto struct_file = parser_->parse();
    QCOMPARE(struct_file->toString().c_str(), "Main: [3*3] a: int8 b, int8 c..");
}

void ParserTest::parseNestedConditionField()
{
    setInput("Main: (True)?: int8 a, (a > 3)?: int8 b...");
    const auto struct_file = parser_->parse();
    QCOMPARE(struct_file->toString().c_str(), "Main: (True)?: int8 a, (a>3)?: int8 b...");
}

void ParserTest::throwExceptionOnUndefinedFieldUse()
{
    setInput("Main: (a == 4)?: int8 c., int8 a.");
    QVERIFY_EXCEPTION_THROWN(parser_->parse(), std::runtime_error);
}

