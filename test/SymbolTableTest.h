#ifndef SYMBOLTABLETEST_H
#define SYMBOLTABLETEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "../lib/ast/SymbolTable.h"

class SymbolTableTest: public QObject
{
    Q_OBJECT
public:
    SymbolTableTest(){}

private slots:
    void registerInt();
    void getRegisteredInt();
    void changeRegisteredIntValue();
    void getUnregisteredInt();
    void getIntRegisteredInParent();
    void getIntUnregisteredInParent();
    void ifIntRegisteredInParentAndChildGetReferenceFromChild();
};

#endif // SYMBOLTABLETEST_H
