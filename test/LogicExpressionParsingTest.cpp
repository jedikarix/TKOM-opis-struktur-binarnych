#include "LogicExpressionParsingTest.h"

#include <QDebug>

void LogicExpressionParsingTest::parseLogicLiteralDirectly()
{
    setInput("True");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicLiteral();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseLogicLiteral()
{
    setInput("False");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();

    QCOMPARE(expr->calculate(), false);
}

void LogicExpressionParsingTest::parseConjunctionOfTrueAndTrue()
{
    setInput("True && True");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseConjunctionOfTrueAndFalseDirectly()
{
    setInput("True && False");
    scanner_->readNextToken();
    const auto expr = parser_->readAlternative();
    QCOMPARE(expr->calculate(), false);
}

void LogicExpressionParsingTest::parseConjunctionOfTrueAndTrueDirectly()
{
    setInput("True && True");
    scanner_->readNextToken();
    const auto expr = parser_->readAlternative();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseConjunctionOfTrueAndFalse()
{
    setInput("True && False");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), false);
}

void LogicExpressionParsingTest::parseConjunctionOfBoolValueAndRelationalExpression()
{
    setInput("True && 3 > 2");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseConjunctionOfTwoRelationalExpressions()
{
    setInput("3 + 2 == 5 && 3 > 2");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseAlternativeOfTrueAndFalse()
{
    setInput("True || False");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseAlternativeOfFalseAndFalse()
{
    setInput("False || False");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), false);
}

void LogicExpressionParsingTest::parseAlternativeOfBoolValueAndConjunction()
{
    setInput("False || (True && 2 > 1)");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseAlternativeOfTwoConjunctions()
{
    setInput("1>2 && 2>3 || 3>4 && 4>5");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), false);
}

void LogicExpressionParsingTest::parseConjunctionOfAlternativeAndBoolValue()
{
    setInput("True && (False || True)");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseConjunctionOfTwoTrueAlternatives()
{
    setInput("(True || False) && (False || True)");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), true);
}

void LogicExpressionParsingTest::parseConjunctionOfTrueAndFalseAlternatives()
{
    setInput("(True || False) && (False || 7+2*4>15)");
    scanner_->readNextToken();
    const auto expr = parser_->readLogicExpression();
    QCOMPARE(expr->calculate(), false);
}











