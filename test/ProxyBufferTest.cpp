#include "ProxyBufferTest.h"

#include <QtDebug>

void ProxyBufferTest::init()
{
    buffer_ = new ProxyBuffer(in_);
}

void ProxyBufferTest::cleanup()
{
    delete buffer_;
}

void ProxyBufferTest::testPeek()
{
    setInput("abc");
    QCOMPARE(buffer_->peek(), 'a');
    QCOMPARE(buffer_->peek(), 'a');
    InputPosition pos = buffer_->getInputPos();
    QCOMPARE(pos.getCharNb(), 1);
}

void ProxyBufferTest::testGetAfterPeek()
{
    setInput("abc");
    QCOMPARE(buffer_->peek(), 'a');
    QCOMPARE(buffer_->get(), 'a');
    InputPosition pos = buffer_->getInputPos();
    QCOMPARE(pos.getCharNb(), 2);
    QCOMPARE(pos.getUFPos().line_, 1);
    QCOMPARE(pos.getUFPos().column_,2);
}

void ProxyBufferTest::testGet()
{
    setInput("abc");
    QCOMPARE(buffer_->get(), 'a');
    QCOMPARE(buffer_->get(), 'b');
    InputPosition pos = buffer_->getInputPos();
    QCOMPARE(pos.getCharNb(), 3);
    QCOMPARE(pos.getUFPos().line_, 1);
    QCOMPARE(pos.getUFPos().column_,3);
    QCOMPARE(buffer_->get(), 'c');
}

void ProxyBufferTest::testIgnoreWhitespaces()
{
    setInput("a \t\nb");
    QCOMPARE(buffer_->get(), 'a');
    buffer_->ignoreWhitespaces();
    QCOMPARE(buffer_->get(), 'b');
    InputPosition pos = buffer_->getInputPos();
    QCOMPARE(pos.getCharNb(), 6);
    QCOMPARE(pos.getUFPos().line_, 2);
    QCOMPARE(pos.getUFPos().column_,2);
}

void ProxyBufferTest::testInputPos()
{
    setInput("a\nb\ncd");
    buffer_->get();
    buffer_->get();
    InputPosition pos = buffer_->getInputPos();
    QCOMPARE(pos.getCharNb(), 3);
    QCOMPARE(pos.getUFPos().line_, 2);
    QCOMPARE(pos.getUFPos().column_,1);
}

void ProxyBufferTest::testEof()
{
    setInput("a");
    buffer_->get();
    buffer_->get();
    QVERIFY(buffer_->eof());
}

