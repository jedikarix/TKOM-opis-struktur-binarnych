#ifndef PARSERTEST_H
#define PARSERTEST_H

#include "ParsingTest.h"

class ParserTest: public ParsingTest
{
    Q_OBJECT
public:
    ParserTest(){}

private slots:
    void parseStructureWithOneField();
    void parseStructureWithIntFieldAndConditionField();
    void parseStructureWithSequence();
    void parseNestedConditionField();
    void throwExceptionOnUndefinedFieldUse();
};

#endif // PARSERTEST_H
