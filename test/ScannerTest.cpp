#include "ScannerTest.h"

#include <iostream>
#include <QDebug>
#include <list>

#include "../lib/parser/AnalysisErrors.h"

using namespace parser;

void ScannerTest::setInput(const std::string &input)
{
    in_.clear();
    in_.str(input);
}

void ScannerTest::init()
{
    buffer_ = new ProxyBuffer(in_);
    scanner_ = new Scanner(*buffer_);
}

void ScannerTest::cleanup()
{
    delete buffer_;
    delete scanner_;

}

void ScannerTest::scanEmpty()
{
    setInput("");
    QCOMPARE(scanner_->getToken().getType(), Token::Type::Eof);
}

void ScannerTest::scanDot() {
    setInput(".");
    scanner_->readNextToken();
    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Dot);
}

void ScannerTest::scanColon() {
    setInput(":");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Colon);
}

void ScannerTest::scanComma() {
    setInput(",");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Comma);
}

void ScannerTest::scanLeftBracket() {
    setInput("(");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Left_bracket);
}

void ScannerTest::scanRightBracket() {
    setInput(")");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Right_bracket);
}

void ScannerTest::scanCondMark() {
    setInput("?");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Cond_mark);
}

void ScannerTest::scanGreater() {
    setInput(">");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Greater);
}

void ScannerTest::scanGreaterEqual() {
    setInput(">=");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Greater_equal);
}

void ScannerTest::scanLess() {
    setInput("<");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Less);
}

void ScannerTest::scanLessEqual() {
    setInput("<=");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Less_equal);
}

void ScannerTest::scanNotEqual() {
    setInput("!=");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Not_equal);
}

void ScannerTest::scanEqual() {
    setInput("==");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Equal);
}

void ScannerTest::scanDisjunction() {
    setInput("||");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Disjunction);
}

void ScannerTest::scanConjunction() {
    setInput("&&");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Conjunction);
}

void ScannerTest::scanPlus() {
    setInput("+");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Plus);
}

void ScannerTest::scanMinus() {
    setInput("-");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Minus);
}

void ScannerTest::scanMultiplication() {
    setInput("*");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Multiplication);
}

void ScannerTest::scanFloorDivision() {
    setInput("/-");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Floor_division);
}

void ScannerTest::scanCeilDivision() {
    setInput("/+");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Ceil_division);
}

void ScannerTest::scanModulo() {
    setInput("%");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Modulo_division);
}

void ScannerTest::scanNegation() {
    setInput("!");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Negation);
}

void ScannerTest::scanReference() {
    setInput("->");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Reference);
}

void ScannerTest::scanLeftSqBracket() {
    setInput("[");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Left_sq_bracket);
}

void ScannerTest::scanRightSqBracket() {
    setInput("]");
    scanner_->readNextToken();

    QVERIFY(scanner_->getToken().getOperator() == ast::Operator::Right_sq_bracket);
}

void ScannerTest::scanBadOperators()
{
    setInput("#$@");
    QVERIFY_EXCEPTION_THROWN(scanner_->readNextToken(), ScannerException);
}

void ScannerTest::scanGoodValue()
{
    setInput("15000");
    scanner_->readNextToken();
    QCOMPARE(scanner_->getToken().getIntVal(), 15000);
}

void ScannerTest::scanTooBigValue()
{
    setInput("123123123123123123123123123123");
    QVERIFY_EXCEPTION_THROWN(scanner_->readNextToken(), ScannerException);
}

void ScannerTest::scanIntKeyword()
{
    setInput("int8");
    scanner_->readNextToken();
    QVERIFY(scanner_->getToken().getType() == Token::Type::Type_int);
    QVERIFY(scanner_->getToken().getValueType() == ast::ValueType::Int8);
}

void ScannerTest::scanStrKeyword()
{
    setInput("str");
    scanner_->readNextToken();
    QVERIFY(scanner_->getToken().getType() == Token::Type::Type_string);
    QVERIFY(scanner_->getToken().getValueType() == ast::ValueType::String);
}


void ScannerTest::scanIdentifier()
{
    setInput("identifier");
    scanner_->readNextToken();
    QVERIFY(scanner_->getToken().getType() == Token::Type::Ident);
    QVERIFY(scanner_->getToken().getIdentifier() == "identifier");
}

void ScannerTest::scanTooLongIdentifier()
{
    setInput("ABCDEFGHIJKLMNOPRSTUVWXYZABCDEFGHIJKLMNOPRSTUVWXYZ");
    QVERIFY_EXCEPTION_THROWN(scanner_->readNextToken(), ScannerException);
}
