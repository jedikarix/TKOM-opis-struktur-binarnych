#include "LogicBinaryExpressionTest.h"

using namespace ast;

void LogicBinaryExpressionTest::calculateConjunctionOfTrueAndTrue()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(true),
                             Operator::Conjunction,
                              std::make_unique<BoolValue>(true));
    QCOMPARE(exp.calculate(), true);
}

void LogicBinaryExpressionTest::calculateConjunctionOfTrueAndFalse()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(true),
                             Operator::Conjunction,
                              std::make_unique<BoolValue>(false));
    QCOMPARE(exp.calculate(), false);
}

void LogicBinaryExpressionTest::calculateConjunctionOfFalseAndTrue()
{
    LogicBinaryExpression exp3(std::make_unique<BoolValue>(false),
                             Operator::Conjunction,
                              std::make_unique<BoolValue>(true));
    QCOMPARE(exp3.calculate(), false);
}

void LogicBinaryExpressionTest::calculateConjunctionOfFalseAndFalse()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(false),
                             Operator::Conjunction,
                              std::make_unique<BoolValue>(false));
    QCOMPARE(exp.calculate(), false);
}

void LogicBinaryExpressionTest::calculateDisjunctionOfFalseAndFalse()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(false),
                             Operator::Disjunction,
                              std::make_unique<BoolValue>(false));
    QCOMPARE(exp.calculate(), false);
}

void LogicBinaryExpressionTest::calculateDisjunctionOfTrueAndTrue()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(true),
                             Operator::Disjunction,
                              std::make_unique<BoolValue>(true));
    QCOMPARE(exp.calculate(), true);
}

void LogicBinaryExpressionTest::calculateDisjunctionOfTrueAndFalse()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(true),
                             Operator::Disjunction,
                              std::make_unique<BoolValue>(false));
    QCOMPARE(exp.calculate(), true);
}

void LogicBinaryExpressionTest::calculateDisjunctionOfFalseAndTrue()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(false),
                             Operator::Disjunction,
                              std::make_unique<BoolValue>(true));
    QCOMPARE(exp.calculate(), true);
}

void LogicBinaryExpressionTest::calculateDisjunctionOfBoolValueAndLogicBinaryExpression()
{
    std::unique_ptr<LogicBinaryExpression> exp =
            std::make_unique<LogicBinaryExpression>(std::make_unique<BoolValue>(true),
                                               Operator::Conjunction,
                                               std::make_unique<BoolValue>(false));
    LogicBinaryExpression exp2(std::move(exp),
                               Operator::Disjunction,
                               std::make_unique<BoolValue>(true));
    QCOMPARE(exp2.calculate(), true);

}

void LogicBinaryExpressionTest::calculateDisjunctionOfBoolValueAndRelationalExpression()
{
    std::unique_ptr<RelationalExpression> exp =
            std::make_unique<RelationalExpression>(std::make_unique<IntValue>(1),
                                                   Operator::Equal,
                                                   std::make_unique<IntValue>(3));
    LogicBinaryExpression exp2(std::move(exp),
                               Operator::Disjunction,
                               std::make_unique<BoolValue>(true));
    QCOMPARE(exp2.calculate(), true);
}

void LogicBinaryExpressionTest::ifNonLogicalOperatorThrowException()
{
    LogicBinaryExpression exp(std::make_unique<BoolValue>(false),
                             Operator::Dot,
                             std::make_unique<BoolValue>(false));
    QVERIFY_EXCEPTION_THROWN(exp.calculate(), std::runtime_error);
}
