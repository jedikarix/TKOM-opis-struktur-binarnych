#ifndef LOGICBINARYEXPRESSIONTEST_H
#define LOGICBINARYEXPRESSIONTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include "../lib/ast/RelationalExpression.h"
#include "../lib/ast/BoolValue.h"
#include "../lib/ast/LogicBinaryExpression.h"
#include "../lib/ast/IntValue.h"

class LogicBinaryExpressionTest: public QObject
{
    Q_OBJECT
public:
    LogicBinaryExpressionTest(){}

private slots:

    void calculateConjunctionOfTrueAndTrue();
    void calculateConjunctionOfTrueAndFalse();
    void calculateConjunctionOfFalseAndTrue();
    void calculateConjunctionOfFalseAndFalse();

    void calculateDisjunctionOfTrueAndTrue();
    void calculateDisjunctionOfTrueAndFalse();
    void calculateDisjunctionOfFalseAndTrue();
    void calculateDisjunctionOfFalseAndFalse();

    void calculateDisjunctionOfBoolValueAndLogicBinaryExpression();
    void calculateDisjunctionOfBoolValueAndRelationalExpression();
    void ifNonLogicalOperatorThrowException();
};

#endif // LOGICBINARYEXPRESSIONTEST_H
