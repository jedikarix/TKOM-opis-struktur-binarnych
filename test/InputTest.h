#ifndef INPUTTEST_H
#define INPUTTEST_H

#include <QObject>
#include <QtTest/QtTest>

#include <string>
#include <sstream>

class InputTest: public QObject
{
    Q_OBJECT
public:
    InputTest() = default;

    void setInput(const std::string &input);

protected:
    std::stringstream in_;
};

#endif // INPUTTEST_H
