#ifndef RELATIONALEXPRESSIONTEST_H
#define RELATIONALEXPRESSIONTEST_H

#include <QObject>
#include <QtTest/QtTest>


#include "../lib/ast/RelationalExpression.h"
#include "../lib/ast/IntValue.h"

class RelationalExpressionTest: public QObject
{
    Q_OBJECT
public:
    RelationalExpressionTest(){}

private slots:
    void checkEqualForIntVals();
    void checkNotEqualForIntVals();
    void checkGreaterForIntVals();
    void checkGreaterEqualForIntVals();
    void checkLessForIntVals();
    void checkLessEqualForIntVals();
    void ifNonRelationalOperatorThrowException();
    void printsProperlyString();
};

#endif // RELATIONALEXPRESSIONTEST_H
